import * as express from 'express';
import { Router, Express } from 'express'
import { RouteConstructor, EndpointContructor } from './decorator/route-constructor';
import { Environment } from './environment/environment';
import { WinterApplication } from './winter-application';

export class Server {
    private static instance:Server
    // private environments:Environment[] = [new Environment()]
    private _environment:Environment
    private _serverRoutes:Function[] = []
    private _errors:any[] = []

    routes:RouteConstructor[] = []

    get app():Express {
        return this._app
    }

    get serverRoutes():Function[] {
        return this._serverRoutes
    }

    get environment():Environment {
        let env = this._environment

        if(!env){
            env = this._environment = WinterApplication.getInstance().environment
        }
        
        return env
    }

    static getInstance():Server {
        if(!this.instance){
            this.instance = new Server()
        }

        return this.instance
    }

    // static setEnvironments(environments:Environment[]){
    //     this.getInstance().environments = environments
    // }

    static addError(error:any){
        Server.getInstance()._errors.push(error)
    }

    // getEnvironment():Environment {
    //     const env = process.env.NODE_ENV || 'dev'

    //     return this.environments.find(e => e.env === env)
    // }

    private constructor(private _app:Express = express()) {
        this._app = _app
        this._serverRoutes.push(this.healthRouter)
        this._serverRoutes.push(this.errorRouter)
    }

    init(){
        console.log('creating server. . .')
        const app = this.app
        
        
        // const PORT = this.getEnvironment().port
        const PORT = this.environment.port
        
        // app.use(this.healthRouter())
        
        app.listen(PORT, () => {
            console.log(`Winter listening on port ${PORT}`);
        });
    }

    private healthRouter():Router {
        const healthRouter:Router = Router()
        const healthResponse = (req, res, next) => {
            res.send({
                status: "OK",
                timestamp: new Date()
            })
        }

        const env = this.environment
        const pathBase = (env.pathBase && env.pathBase.length > 0) ? `/${env.pathBase}` : ''
        const healthPath = `${pathBase}/${env.health}`
        healthRouter.get(healthPath, healthResponse)

        console.log(`get - ${healthPath}`)

        this._app.use(healthRouter)

        return healthRouter
    }

    private errorRouter(): Router {
        const env = this.environment
        const pathBase = (env.pathBase && env.pathBase.length > 0) ? `/${env.pathBase}` : ''
        const errorPath = `${pathBase}/errors`

        const errorRouter:Router = Router()

        errorRouter.get(errorPath, (req, res) => res.send(JSON.stringify(this._errors)))

        console.log(`get - ${errorPath}`)
        this._app.use(errorRouter)

        return errorRouter      
    }

}

export interface ServerRouter {
    path:string
    router:Router
}
