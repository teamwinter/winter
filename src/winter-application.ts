import { RoutesConstructor } from './decorator/route-constructor';
import { Injector } from './decorator/injector';
import { Server } from './server';
import { Environment } from './environment/environment';

export class WinterApplication {
    private _injector:Injector = new Injector()
    private _routeConstructor:RoutesConstructor = new RoutesConstructor()
    private _server:Server = Server.getInstance()
    private _environments:Environment[] = [new Environment()]
    private static instance:WinterApplication
   
    static getInstance(){
        if(!this.instance){
            this.instance = new WinterApplication()
        }

        return this.instance
    }

    get environment():Environment {
        const env = process.env.NODE_ENV || 'dev'

        return this._environments.find(e => e.env === env)
    }

    static set environments(environments:Environment[]){
        this.instance._environments = environments
    }

    get routeConstructor():RoutesConstructor {
        return this._routeConstructor
    }

    get injector():Injector {
        return this._injector
    }

    get server():Server {
        return this._server
    }

    createControllers(controllers:any[]){
        const winter = WinterApplication.getInstance()
    
        controllers.forEach(controller => {
            let instance = winter.injector.injections.get(controller.name)
            console.log(`construct routes for ${controller.name}`)
            winter.routeConstructor.createRoutes(instance)
        })
    }

}