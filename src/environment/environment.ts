export class Environment {
    env: 'dev' | 'stg' | 'preprod' | 'prod' = 'dev'
    pathBase?:string = 'api'
    port?: string = '4000'
    health?: string = 'healthz'
    database?: DatabaseConfig = new DatabaseConfig()
    ngUniversal?: NgUniversalConfig = new NgUniversalConfig()   
}

export class DatabaseConfig {
    mongodb?:MongoDbConfig = new MongoDbConfig()
}

export class MongoDbConfig {
    host?: string = 'localhost'
    port?: string = '27017'
    authDb?: string = 'test'
    connectionDb?: string = 'test'
}

export class NgUniversalConfig {
    dist?:string = 'dist'
    browser?:string = 'browser'
    server?:string = 'server/main'
}