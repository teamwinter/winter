import { Express } from 'express'
import { Environment } from '../environment/environment';

export interface WinterPlugin {
    apply(app:Express, environment:Environment, express)
}