import { DecoratorHelper } from "./create-decorator";
import { Server } from "../server";
import * as express from 'express'
import { WinterPlugin } from "../plugins/plugin";
import { Environment } from "../environment/environment";
import { RoutesConstructor } from "./route-constructor";
import { WinterApplication } from "../winter-application";

export function Winter(params:WinterParams) {
    return (target: Function) => {
        const winter = WinterApplication.getInstance()
        
        params = resolveParams(params)
        WinterApplication.environments = resolveEnvironments(params.environments)
        winter.createControllers(params.controllers)

        const server = winter.server
        
        console.log('adding routes')
        winter.routeConstructor.routesToAdd.forEach(route => {
            route.apply(RoutesConstructor)
        })

        console.log('applying server routes')
        server.serverRoutes.forEach((serverRoute) => {
            serverRoute.apply(server)
        })

        if(params.plugins){
            console.info('applying plugins . . . ')
            params.plugins.forEach(plugin => 
                plugin.apply(server.app, winter.environment, express)
            )
        }

        server.init()
        
        let helper = new DecoratorHelper(target, params)
        
        var f: any = function (...args) {
            console.log("New: " + helper.target.name);
            let object = helper.construct(args);
            object.params = params
            return object
        }

        f.prototype = helper.target.prototype;

        return f
    }
}

function resolveEnvironments(environments:Environment[]){
    console.log("envs", environments)
    let resolvedEnvironments = environments
    const defaultEnv = new Environment()
    
    if(!environments || environments.length === 0){
        resolvedEnvironments = [defaultEnv]
    } else {
        console.log('current env', resolvedEnvironments)
        resolvedEnvironments = resolvedEnvironments.map(env => {
            return Object.assign({}, defaultEnv, env)
        })
        console.log('result env', resolvedEnvironments)
    }
    console.log('resolving env', resolvedEnvironments)
    return resolvedEnvironments
}

function resolveParams(params:WinterParams):WinterParams {
    let paramsWithDefaults:WinterParams = params
    paramsWithDefaults.services = params.services ? params.services : []
    paramsWithDefaults.controllers = params.controllers ? params.controllers : []
    paramsWithDefaults.connectors = params.connectors ? params.connectors : []
    
    return paramsWithDefaults
}

export interface WinterParams {
    controllers?:any[]
    services?:any[],
    plugins?:WinterPlugin[],
    environments?:Environment[],
    connectors?:any[]
}

