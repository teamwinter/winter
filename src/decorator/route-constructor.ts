import { Router, RequestHandler, Request, Response } from "express";
import { NextFunction } from "connect";
import { Server } from "../server";
import { RequestMethod } from "../rest-request-method";
import { ControllerMethodMetadata } from "./controller.decorator";
import { WinterApplication } from "../winter-application";

export class RoutesConstructor {
    private routes:RouteConstructor[] = []
    routesToAdd: Function[] = []

    mapRoute(controller:string, path:string){
        let route = this.routes.find(route => route.controller === controller)
        
        if(!route){
            route = new RouteConstructor(controller, path, [])
            this.routes.push(route)
        }

        route.path = path
    }

    mapEndpoint(
        controller:string, 
        method:RequestMethod, 
        path:string, 
        requestHandler:RequestHandler[],
        metadata:ControllerMethodMetadata
    ){
        let route = this.routes.find(route => route.controller === controller)

        if(!route){
            route = new RouteConstructor(controller)
            this.routes.push(route)
        }

        route.endpoints.push(new EndpointContructor(method, path, requestHandler, metadata)) 
    }


    createRoutes(controllerInstance) {
        this.routesToAdd.push(() => {
            let controllerName = controllerInstance.constructor.name
            const route:RouteConstructor = this.routes.find(route => route.controller === controllerName)
            const pathBase = WinterApplication.getInstance().environment.pathBase//Server.getInstance().getEnvironment().pathBase

            const router = Router()

            console.info(`creating route for ${controllerName}`)

            route.endpoints.forEach(endpoint => {
                let method = RequestMethod[endpoint.method].toLocaleLowerCase()

                let path = `/${route.path}${endpoint.path}`

                if (pathBase.length > 0) {
                    path = `/${pathBase}${path}`
                }       

                const callback: RequestHandler = (req: Request, res: Response, next: NextFunction) => {
                    return function(){
                        let args = []
                        endpoint.metadata.paramNames.forEach(key => {
                            let param = req.params[key]
                            if(param !== undefined && param !== null){
                                args.push(param)
                            } else {
                                let query = req.query[key]
                                if(param !== undefined && param !== null){
                                    args.push(query)
                                }
                            }                            
                        })

                        const originalCallResult:Promise<any> = (<any>endpoint.requestHandler)
                            .apply(controllerInstance, args)
                        
                        originalCallResult
                            .then(result => {
                                res.send(result)
                            })

                    }.apply(controllerInstance, [req, res, next])
                }

                console.info(`${method} - ${path}`)

                router[method](path, callback)
            })

            Server.getInstance().app.use(router)
        })
    }

    private resolveRoute(controller: string): RouteConstructor {
        const routes = Server.getInstance().routes

        let route = routes.find(route => route.controller === controller)

        if (!route) {
            route = new RouteConstructor(controller)
            routes.push(route)
        }

        return route
    }

}


export class RouteConstructor {
    constructor(
        public controller?: string,
        public path?: string,
        public endpoints: EndpointContructor[] = []
    ) { }
}

export class EndpointContructor {
    constructor(
        public method: RequestMethod,
        public path: string,
        public requestHandler: RequestHandler[],
        public metadata: ControllerMethodMetadata
    ) { }
}