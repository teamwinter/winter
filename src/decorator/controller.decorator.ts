import 'reflect-metadata'
import { RequestMethod } from '../rest-request-method';
import { WinterApplication } from '../winter-application';

export function ControllerApi(controller:Object){
    return function(target){
        Object.keys(controller)
            .forEach(key => {
                console.log(key, controller[key])
                Object.keys(key)
                    .forEach(k => console.log(k, key[k]))
            })
    }
}

export function Controller(config: string | { path:string, api?:any }) {
    return function (target: any) {
        let path:string = ''
        let api

        if(typeof config === 'string'){
            path = config
        } else {
            path = config.path
            api = config.api
            initControllerMetadata(api)
            api.__metadata__['path'] = path        
        }

        initControllerMetadata(target)
        
        target.__metadata__['path'] = path        
        
        const winter = WinterApplication.getInstance()
        
        winter.routeConstructor.mapRoute(target.name, path)
        winter.injector.createInjecteeInstance(target)
    }
}

export const Get = (path: string) => {
    return (target: Object, key: string, descriptor: TypedPropertyDescriptor<any>) => {
        
        let type = Reflect.getMetadata("design:type", target, key);
        let types = Reflect.getMetadata("design:paramtypes", target, key);
        
        restMethodDecorator(RequestMethod.Get, path, target, key, descriptor)
    }
}

export const Post = (path: string) => {
    return (target: Object, key: string, descriptor: PropertyDescriptor) => {
        restMethodDecorator(RequestMethod.Post, path, target, key, descriptor)
    }
}

export const Put = (path: string) => {
    return (target: Object, key: string, descriptor: PropertyDescriptor) => {
        restMethodDecorator(RequestMethod.Put, path, target, key, descriptor)
    }
}

export const Patch = (path: string) => {
    return (target: Object, key: string, descriptor: PropertyDescriptor) => {
        restMethodDecorator(RequestMethod.Patch, path, target, key, descriptor)
    }
}

export const Delete = (path: string) => {
    return (target: Object, key: string, descriptor: PropertyDescriptor) => {
        restMethodDecorator(RequestMethod.Delete, path, target, key, descriptor)
    }
}


const restMethodDecorator = (
    method: RequestMethod,
    path: string,
    target: any,
    key: string,
    descriptor: PropertyDescriptor) => {
    
    // save a reference to the original method this way we keep the values currently in the
    // descriptor and don't overwrite what another decorator might have done to the descriptor.
    if (descriptor === undefined) {
        descriptor = Object.getOwnPropertyDescriptor(target, key)
    }

    let controllerName = target.constructor.name

    let metadata = new ControllerMethodMetadata(method, path, key, 
        getMethodParamNames(<any>descriptor.value), getArgs(descriptor.value))
    addMethodMetadata(controllerName, metadata)
    
    WinterApplication
        .getInstance().routeConstructor
        .mapEndpoint(controllerName, method, path, descriptor.value, metadata)
}

function addMethodMetadata(controllerName:any, metadata:ControllerMethodMetadata){
    MetadataAdmin.getInstance()
        .addMethod(controllerName, metadata)
}

function initControllerMetadata(target){
    if(!target.hasOwnProperty('__metadata__')){
        target.__metadata__ = new ControllerMetadata()
        target.__metadata__['methods'] = MetadataAdmin.getInstance()
            .getMethods(target.name)
            
    }
}

function getMethodParamNames(method: Function): string[] {

    let funcString = method.toString()

    let paramsString = funcString.split('function (')[1].split(')')[0]

    return paramsString.split(',')
        .map(param => param.trim())
}



function getArgs(Class) {
    let args = []
    var types = Reflect.getMetadata("design:paramtypes", Class);
    if (types) {
        var s = types.map(a => a.name);
        args.push(s[0])
    }
    return args
}

export class ControllerMetadata {
    path:string
    methods:ControllerMethodMetadata[] = []
}

export class ControllerMethodMetadata {
    
    constructor(
        public method:RequestMethod,
        public path:string,
        public key:string,
        public paramNames?:string[],
        public paramTypes?:string[]
    ){}
}


export class MetadataAdmin {
    private static instance:MetadataAdmin
    private metadatas:Map<string, ControllerMethodMetadata[]> = new Map()

    static getInstance():MetadataAdmin {
        if(!MetadataAdmin.instance){
            MetadataAdmin.instance = new MetadataAdmin()
        }
        return MetadataAdmin.instance
    }

    addMethod(controllerName:string, method:ControllerMethodMetadata){
        let metadata

        if(this.metadatas.has(controllerName)){
            metadata = this.metadatas.get(controllerName)
        } else {
            metadata = []
        }

        metadata.push(method)

        this.metadatas.set(controllerName, metadata)
    }

    getMethods(controllerName:string):ControllerMethodMetadata[]{
        return this.metadatas.get(controllerName)
    }
    private constructor(){}

}


