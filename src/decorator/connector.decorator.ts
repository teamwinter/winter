import 'reflect-metadata'
import { ControllerMetadata } from './controller.decorator';
import { WinterApplication } from '../winter-application';
import { get } from 'http'

export function Connector(path?: string) {
    return function (target: any) {
        const prototype = Reflect.getPrototypeOf(target)
        
        const metadata:ControllerMetadata = prototype['__metadata__']

        metadata.methods.forEach(m => {
            
            target.prototype[m.key] = function(...args){
                let req = analyzePath(m.path)
                let resultPath = path + '/' + metadata.path + m.path

                req.params.forEach(param => {
                    let replace = `:${param}`
                    let replacing = args[m.paramNames.findIndex(p => p === param)]
                    resultPath = resultPath.replace(replace, replacing)
                })

                req.query.forEach(query => {
                    let replace = `${query}=`
                    let replacing = args[m.paramNames.findIndex(p => p === query)]
                    if(replacing){
                        resultPath = resultPath.replace(replace, `${query}=${replacing}`)
                    }
                })

                let promise = new Promise(resolve => {
                    get(resultPath, response => {
                        let data = ''
                        response.on('data', chunk => data += chunk)
                        response.on('end', () => {
                            resolve(JSON.parse(data))
                        })
                    })
                })

                return promise
            }
        })

        WinterApplication
            .getInstance()
            .injector
            .createInjecteeInstance(target)

    }
}
// :{ params:string[], query:string[] }
function analyzePath(path:string){
    let req = {
        params: [],
        query: []
    }

    let paramsPath
    let queryPath
    
    if(path.includes('?')){
        let split = path.split('?')
        paramsPath = split[0]
        queryPath = split[1]
    } else {
        paramsPath = path
    }

    let paramsRegion:string[] = paramsPath.split(':')
    req.params = paramsRegion
        .filter(region => region.length > 0)
        .map((region:string) => region.split('/')[0])
        .filter(region => region.length > 0)

    let queryRegion = queryPath ? queryPath.split('&') : []
    req.query = queryRegion
        .filter(region => region.length > 0)
        .map(region => region.split('=')[0])
        .filter(region => region.length > 0)

    return req
}

function getArgs(Class) {
    let args = []
    var types = Reflect.getMetadata("design:paramtypes", Class);
    if (types) {
        var s = types.map(a => a.name);
        args.push(s[0])
    }
    return args
}