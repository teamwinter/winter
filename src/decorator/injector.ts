import 'reflect-metadata'
import { Constructable, invokeConstructor} from "./util";

export class Injector {
    injections: Map<string, Object> = new Map()

    createInjecteeInstance<T>(Class: Constructable<T>): T {
        console.log(`creating ${Class.name} . . .`)
        let result: T

        result = invokeConstructor(Class, this.getArgs(Class))

        this.injections.set(Class.name, result)

        return result
    }

    getArgs(Class) {
        let args = []
        var types = Reflect.getMetadata("design:paramtypes", Class);
        if (types) {
            var s = types.map(a => a.name);
            args.push(this.injections.get(s[0]))
        }
        return args
    }

}
