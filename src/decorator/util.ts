
export function invokeConstructor<T>(Class: Constructable<T>, args:Array<any>): T {
    switch(args.length){
        case 0 : return new Class()
        case 1 : return new Class(args[0])
        case 2 : return new Class(args[0], args[1])
        case 3 : return new Class(args[0], args[1], args[2])
        case 4 : return new Class(args[0], args[1], args[2], args[3])
        case 5 : return new Class(args[0], args[1], args[2], args[3], args[4])
        case 6 : return new Class(args[0], args[1], args[2], args[3], args[4], args[5])
        case 7 : return new Class(args[0], args[1], args[2], args[3], args[4], args[5], args[6])
        case 8 : return new Class(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7])
    }
}


export interface Constructable<T>{
    new(...args: any[]):T
}

export const CONSTRUCTOR_PROPERTY_NAME = 'constructor'

export function logTypes(target: any) {
    var types = Reflect.getMetadata("design:paramtypes", target);
    if (types) {
        var s = types.map(a => a.name).join();
    }
}
