import { WinterApplication } from "../winter-application";

export function Service(){
    return (target: any) => {
        const winter = WinterApplication.getInstance()
        winter.injector.createInjecteeInstance(target)

        return target
    }   
}


