export class DecoratorHelper {
    constructor(public target:any, public extraArgs){
    }

    construct(args) {
        var c: any = function () {
            return this.target.apply(this, args);
        }
        c.prototype = this.target.prototype;
        return new c();
    }
}