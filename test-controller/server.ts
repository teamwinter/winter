import { Winter } from "../dist";
import { TestConnectorController } from "./test-controller";
import { DevEnv } from "./environments/dev.env";
import { TestConnector } from "./test.connector";

@Winter({
    controllers: [
        TestConnectorController
    ],
    plugins: [
    
    ],
    environments: [
        DevEnv
    ],
    connectors: [
       TestConnector 
    ],
    services: [
    
    ]
})
export class Server {

}
