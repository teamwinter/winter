import { Controller, Get } from "../dist";
import { Request } from "express";
import { TestConnector } from "./test.connector";


@Controller('connect')
export class TestConnectorController {
    field="hola"

    @Get('/:id')
    getAll(id:string) {
        return this.testConnector.getAll(id)
    }

    constructor(private testConnector:TestConnector){

    }

}

// {
//     path : "test",
//     methods: [
//         {
//             getAll: {
//                 method: "GET",
//                 path: '/:id?page=2&age=12'
//             }
//         }
        
//     ]
// }

// http://api.loca.com/v1/test/1?page=2&age=23

// new TestController().getAll('1', 2, 23)