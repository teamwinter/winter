import { Environment } from '../../dist'

export const DevEnv:Environment = {
    env : 'dev',
    pathBase: 'api',
    port : '4002'
    
}