import { Service } from '../dist'

@Service()
export class TestService {

    getSomething(){
        return 'something from test service'
    }

}