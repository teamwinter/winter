import { Controller, Get, Post } from "../dist";

@Controller('test2')
export class Test2Controller {

    @Get('')
    getAll(req, res, next){
        res.send('all test 2')
    }

    @Post('')
    postAll(req, res, next){
        res.send('all test')
    }
}