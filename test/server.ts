import { Winter } from "../dist";
import { TestController } from "./test-controller";
import { DevEnv } from "./environments/dev.env";
import { Test2Controller } from "./test2-controller";
import { TestService } from "./test.service";
import { Test2Service } from "./test.service.1";
import { Test3Service } from "./test.service.2";

@Winter({
    controllers: [
        TestController,
        Test2Controller
    ],
    plugins: [
    ],
    environments: [
        DevEnv
    ],
    services: [
        TestService,
        Test2Service,
        Test3Service    
    ]
})
export class Server {

}