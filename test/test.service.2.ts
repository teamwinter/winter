import { Service } from '../dist'
import { TestService } from './test.service';

@Service()
export class Test3Service {

    getSomething(){
        return 'something from test service'
    }

    constructor(private testService:TestService){}
}