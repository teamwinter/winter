import { Controller, Get } from "../dist";
import { TestService } from "./test.service";
import { Song } from './song'

@Controller('test')
export class TestController {
    map:Map<string, Song> = new Map()
        .set('1', new Song("Un vestido y un amor", "Fito Paez"))
        .set('2', new Song("Muchacha ojos de papel", "Luis Alberto Spinetta"))
        .set('3', new Song("Flaca", "Andrés Calamaro"))
        .set('4', new Song("Demoliendo hoteles", "Charly García"))


    @Get('/:id')
    getAll(id:string):Promise<Song> {
        let promise:Promise<Song> = new Promise((resolve) => {
            console.log('resolving song', id)
            resolve(this.map.get(id))
        })

        return promise
    }

    constructor(private testService:TestService){
    }

}

