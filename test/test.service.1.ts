import { Service } from '../dist'
import { Test3Service } from './test.service.2';

@Service()
export class Test2Service {

    getSomething(){
        return 'something from test service'
    }

    constructor(private test3Service:Test3Service){

    }
}