# Winter
Winter is a layer over the express framework to provide some extra features, like dependency injection, convention over configuration and declarative endpoints.

## Project status
The project it's a provee of concept.

## Quickstart

### Install the package

You must to have installed some libraries before:

```
$ npm i -g typescript ts-node
```

Then install the framework:

```
$ npm i -s winter-core
```

### The source tree

```
src
 |- server.ts
 |- test.controller.ts
package.json
```

### Create a test.controller.ts

```
import { Controller, Get } from 'winter-core'

@Controller('test')
export class TestController {

    @Get('/:id')
    getAll(id:string):Promise<string> {
        return Promise.resolve(`your id is ${id}`)
    }
}
```

### Create server.ts

```
import { Winter } from "winter-core";
import { TestController } from "./test.controller";

@Winter({
    controllers: [
        TestController
    ]
})
export class Server {

}
```

### Excecute the code

```
$ ts-node src/server.ts
```